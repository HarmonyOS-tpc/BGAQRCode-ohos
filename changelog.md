# 修改的功能
版本1.0.1
### 支持功能：
* 常规的条码、二维码等扫描识别
* 支持全屏扫描（只识别扫描框区域内码暂不支持）
* 可以控制闪光灯，方便夜间使用
* 默认后置摄像头识别（前置摄像头扫描测试中，暂未开放）
* 目前zbar库，底层用的YUVY格式转图片，其他的格式待继续调试

### 暂不支持的功能
* 不支持功能，会影响使用的几个点，待继续完善：
* 扫描预览时，二指缩放预览暂不支持
* 识别图库中的条码、二维码图片暂不支持。（需要获取系统相册列表，研究中）
* 摄像头自动对焦未实现，研究中
* 识别到比较小的码时自动放大
* deveco studio暂不支持以cn开头的项目包名，暂时修改为com

### 扫描框暂不支持的自定义属性
* qrcv_tipText
* qrcv_tipTextSize
* qrcv_tipTextColor
* qrcv_tipTextBelowRect
* qrcv_tipTextMargin
* qrcv_tipShowTipTextAsSingleLine
* qrcv_isShowTipBackground
* qrcv_tipBackgroundColor
* qrcv_isOnlyDecodeScanBoxArea
* qrcv_isShowLocationPoint
* qrcv_isAutoZoom
