/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bingoogolapple.qrcode.core;

/**
 * 字符串工具类
 */
public class TextUtils {
    /**
     * 字符串非空判断
     *
     * @param content 传入的内容
     * @return boolean 是否为空
     */
    public static boolean isEmpty(String content)
    {
        return (content == null || content.length() == 0);
    }
}
