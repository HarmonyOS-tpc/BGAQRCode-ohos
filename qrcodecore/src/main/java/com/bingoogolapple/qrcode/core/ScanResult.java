package com.bingoogolapple.qrcode.core;

/**
 * 解析结果封装类
 */
public class ScanResult {
    String result;
    float[] resultPoints;

    public ScanResult(String result) {
        this.result = result;
    }

    public ScanResult(String result, float[] resultPoints) {
        this.result = result;
        this.resultPoints = resultPoints;
    }
}
