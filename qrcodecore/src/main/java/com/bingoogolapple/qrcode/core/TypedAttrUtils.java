/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bingoogolapple.qrcode.core;

import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Color;

/**
 * AttrSet的util工具类
 *
 * @since 2021-03-24
 */
public class TypedAttrUtils {
    /**
     * 返回自定义int类型的颜色值
     *
     * @param attrs AttrSet类
     * @param attrName 自定义属性名
     * @param defValue 默认值
     * @return 自定义int类型的颜色值
     */
    public static int getIntColor(AttrSet attrs, String attrName, int defValue) {
        if (attrs.getAttr(attrName) != null && attrs.getAttr(attrName).isPresent()) {
            return attrs.getAttr(attrName).get().getColorValue().getValue();
        } else {
            return defValue;
        }
    }

    /**
     * 返回自定义Color类型的颜色值
     *
     * @param attrs AttrSet类
     * @param attrName 自定义属性名
     * @param defValue 默认值
     * @return 自定义Color类型的颜色值
     */
    public static Color getColor(AttrSet attrs, String attrName, Color defValue) {
        if (attrs.getAttr(attrName) != null && attrs.getAttr(attrName).isPresent()) {
            return attrs.getAttr(attrName).get().getColorValue();
        } else {
            return defValue;
        }
    }

    /**
     * 返回自定义布尔值
     *
     * @param attrs AttrSet类
     * @param attrName 自定义属性名
     * @param defValue 默认值
     * @return 自定义布尔值
     */
    public static boolean getBoolean(AttrSet attrs, String attrName, boolean defValue) {
        if (attrs.getAttr(attrName) != null && attrs.getAttr(attrName).isPresent()) {
            return attrs.getAttr(attrName).get().getBoolValue();
        } else {
            return defValue;
        }
    }

    /**
     * 返回自定义字符串值
     *
     * @param attrs AttrSet类
     * @param attrName 自定义属性名
     * @param defValue 默认值
     * @return 自定义字符串值
     */
    public static String getString(AttrSet attrs, String attrName, String defValue) {
        if (attrs.getAttr(attrName) != null && attrs.getAttr(attrName).isPresent()) {
            return attrs.getAttr(attrName).get().getStringValue();
        } else {
            return defValue;
        }
    }

    /**
     * 返回自定义value值
     *
     * @param attrs AttrSet类
     * @param attrName 自定义属性名
     * @param defValue 默认值
     * @return 自定义value值
     */
    public static int getDimensionPixelSize(AttrSet attrs, String attrName, int defValue) {
        if (attrs.getAttr(attrName) != null && attrs.getAttr(attrName).isPresent()) {
            return attrs.getAttr(attrName).get().getDimensionValue();
        } else {
            return defValue;
        }
    }

    /**
     * 返回自定义Integer值
     *
     * @param attrs AttrSet类
     * @param attrName 自定义属性名
     * @param defValue 默认值
     * @return 自定义Integer值
     */
    public static int getInteger(AttrSet attrs, String attrName, int defValue) {
        if (attrs.getAttr(attrName) != null && attrs.getAttr(attrName).isPresent()) {
            return attrs.getAttr(attrName).get().getIntegerValue();
        } else {
            return defValue;
        }
    }

    /**
     * 返回自定义Float值
     *
     * @param attrs AttrSet类
     * @param attrName 自定义属性名
     * @param defValue 默认值
     * @return 自定义Float值
     */
    public static float getFloat(AttrSet attrs, String attrName, float defValue) {
        if (attrs.getAttr(attrName) != null && attrs.getAttr(attrName).isPresent()) {
            return attrs.getAttr(attrName).get().getFloatValue();
        } else {
            return defValue;
        }
    }

    /**
     * 返回自定义Element值
     *
     * @param attrs AttrSet类
     * @param attrName 自定义属性名
     * @param defValue 默认值
     * @return 自定义Element值
     */
    public static Element getElementFromAttr(AttrSet attrs, String attrName, Element defValue) {
        Element value=defValue;
        try{
            if (attrs.getAttr(attrName)!= null&&attrs.getAttr(attrName).isPresent()) {
                value = attrs.getAttr(attrName).get().getElement();
            }
        }catch (Exception e){
            LogUtil.error(e.getMessage());
        }
        return value;
    }
}
