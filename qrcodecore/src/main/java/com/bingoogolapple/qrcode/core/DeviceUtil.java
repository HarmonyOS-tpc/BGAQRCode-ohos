/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bingoogolapple.qrcode.core;
import ohos.agp.utils.Point;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

/**
 * 获取设备信息工具类
 */
public class DeviceUtil {


    /**
     * 获取屏幕上的坐标点
     *
     * @param context 上下文
     * @return 屏幕上的坐标点
     */
    public static Point getDisplayAttributes(Context context) {

        DisplayAttributes displayAttributes = DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes();
        Point size = new Point(displayAttributes.width,displayAttributes.height);
        float xDpi = displayAttributes.xDpi;
        float yDpi = displayAttributes.yDpi;
        float densityDpi = displayAttributes.densityDpi;
        float densityPixels = displayAttributes.densityPixels;
        float scalDensity = displayAttributes.scalDensity;


        LogUtil.error("xDpi:" +xDpi + ",yDpi:" + yDpi + ",densityDpi:" + densityDpi
                +",densityPixels:"+densityPixels+",scalDensity:"+scalDensity);

        LogUtil.error( "screen：width:" + size.getPointX() + ",height:" + size.getPointY());
        return size;
    }

}
