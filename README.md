# BGAQRCode-ohos
支持二维码和条形码的扫描工具库，方便的实现扫码识别和生成二维码的功能

## 目录
* [功能介绍](#功能介绍)
* [演示](#演示)
* [集成](#集成)
* [布局文件](#布局文件)
* [自定义属性说明](#自定义属性说明)
* [接口说明](#接口说明)


## 功能介绍

- [x] 可定制各式各样的扫描框
- [x] 可定制全屏扫描
- [x] 可定制要识别的码的格式
- [x] 可以控制闪光灯，方便夜间使用
- [x] zxing二维码扫描功能
- [x] ZBar 扫描条码、二维码「已解决中文乱码问题」

## 演示

| 扫码页面|识别图中码 |
|:---:|:---:|
|<img src="screenshot/scan.gif" width="40%"/>|<img src="screenshot/picturescan.gif" width="40%"/>|


## 集成

**注意：请将entry中资源目录media下的图片拷贝到自己项目中，否则会报空指针**
```
方式一
添加zxing.har,zbar.har,qrcodecore.har到entry下的libs文件夹内
entry的build.gradle内添加如下代码
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
将zbar模块下编译生成的（libiconv.so,libzbarjni.so）添加到entry的libs包中

方式二
使用远程依赖方式，classpath最低版本要求是2.4.2.7
allprojects {
    repositories {
    mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:BGAQRCode-zbar:1.0.6'
implementation 'io.openharmony.tpc.thirdlib:BGAQRCode-zxing:1.0.5'
implementation 'io.openharmony.tpc.thirdlib:BGAQRCode-core:1.0.5'
```


## 布局文件
>ZXing

```xml
 <com.bingoogolapple.qrcode.zxing.ZXingView
            ohos:height="match_parent"
            ohos:id="$+id:zxingview"
            ohos:width="match_parent"
            app:qrcv_animTime="1000"
            app:qrcv_barcodeRectHeight="120vp"
            app:qrcv_borderColor="#ffffff"
            app:qrcv_borderSize="1vp"
            app:qrcv_cornerColor="#f57257"
            app:qrcv_cornerDisplayType="center"
            app:qrcv_cornerLength="20vp"
            app:qrcv_cornerSize="3vp"
            app:qrcv_customScanLineDrawable="$media:qrcode_default_scan_line"
            app:qrcv_isBarcode="false"
            app:qrcv_isScanLineReverse="true"
            app:qrcv_isShowDefaultGridScanLineDrawable="false"
            app:qrcv_isShowDefaultScanLineDrawable="true"
            app:qrcv_maskColor="#33FFFFFF"
            app:qrcv_rectWidth="200vp"
            app:qrcv_scanLineColor="#f57257"
            app:qrcv_scanLineMargin="0vp"
            app:qrcv_scanLineSize="1vp"
            app:qrcv_toolbarHeight="56vp"
            app:qrcv_topOffset="65vp"
            app:qrcv_verticalBias="-1"/>
```
>ZBar

```xml
 <com.bingoogolapple.qrcode.zbar.ZBarView
            ohos:height="match_parent"
            ohos:id="$+id:zbarview"
            ohos:width="match_parent"
            app:qrcv_animTime="1000"
            app:qrcv_barcodeRectHeight="120vp"
            app:qrcv_borderColor="#ffffff"
            app:qrcv_borderSize="1vp"
            app:qrcv_cornerColor="#f57257"
            app:qrcv_cornerDisplayType="center"
            app:qrcv_cornerLength="20vp"
            app:qrcv_cornerSize="3vp"
            app:qrcv_customScanLineDrawable="$media:qrcode_default_scan_line"
            app:qrcv_isBarcode="false"
            app:qrcv_isScanLineReverse="true"
            app:qrcv_isShowDefaultGridScanLineDrawable="false"
            app:qrcv_isShowDefaultScanLineDrawable="true"
            app:qrcv_maskColor="#33FFFFFF"
            app:qrcv_rectWidth="200vp"
            app:qrcv_scanLineColor="#f57257"
            app:qrcv_scanLineMargin="0vp"
            app:qrcv_scanLineSize="1vp"
            app:qrcv_toolbarHeight="56vp"
            app:qrcv_topOffset="65vp"
            app:qrcv_verticalBias="-1"
                                                />

```

## 自定义属性说明

属性名 | 说明 | 默认值
:----------- | :----------- | :-----------
qrcv_topOffset         | 扫描框距离 toolbar 底部的距离        | 90vp
qrcv_cornerSize         | 扫描框边角线的宽度        | 3vp
qrcv_cornerLength         | 扫描框边角线的长度        | 20vp
qrcv_cornerColor         | 扫描框边角线的颜色        | #ffffff
qrcv_cornerDisplayType         | 扫描框边角线显示位置(相对于边框)，默认值为中间        | center
qrcv_rectWidth         | 扫描框的宽度        | 200vp
qrcv_barcodeRectHeight         | 条码扫样式描框的高度        | 140vp
qrcv_maskColor         | 除去扫描框，其余部分阴影颜色        | #33FFFFFF
qrcv_scanLineSize         | 扫描线的宽度        | 1vp
qrcv_scanLineColor         | 扫描线的颜色「扫描线和默认的扫描线图片的颜色」        | #ffffff
qrcv_scanLineMargin         | 扫描线距离上下或者左右边框的间距        | 0vp
qrcv_isShowDefaultScanLineDrawable         | 是否显示默认的图片扫描线「设置该属性后 qrcv_scanLineSize 将失效，可以通过 qrcv_scanLineColor 设置扫描线的颜色，避免让你公司的UI单独给你出特定颜色的扫描线图片」        | false
qrcv_customScanLineDrawable         | 扫描线的图片资源「默认的扫描线图片样式不能满足你的需求时使用，设置该属性后 qrcv_isShowDefaultScanLineDrawable、qrcv_scanLineSize、qrcv_scanLineColor 将失效」        | null
qrcv_borderSize         | 扫描边框的宽度        | 1vp
qrcv_borderColor         | 扫描边框的颜色        | #ffffff
qrcv_animTime         | 扫描线从顶部移动到底部的动画时间「单位为毫秒」        | 1000
qrcv_verticalBias         | 扫描框中心点在屏幕垂直方向的比例，当设置此值时，会忽略 qrcv_topOffset 属性        | -1
qrcv_toolbarHeight         | Toolbar 的高度，通过该属性来修正由 Toolbar 导致扫描框在垂直方向上的偏差        | 0dp
qrcv_isBarcode         | 扫描框的样式是否为扫条形码样式        | false
qrcv_isScanLineReverse         | 扫描线是否来回移动        | true
qrcv_isShowDefaultGridScanLineDrawable         | 是否显示默认的网格图片扫描线        | false
qrcv_customGridScanLineDrawable         | 扫描线的网格图片资源        | nulll


 ## 暂不支持的自定义属性
  属性名 | 说明 | 默认值
  :----------- | :----------- | :-----------
 qrcv_tipText         | 提示文案        | null
 qrcv_tipTextSize         | 提示文案字体大小        | 14fp
 qrcv_tipTextColor         | 提示文案颜色        | #ffffff
 qrcv_isTipTextBelowRect         | 提示文案是否在扫描框的底部        | false
 qrcv_tipTextMargin         | 提示文案与扫描框之间的间距        | 20vp
 qrcv_isShowTipTextAsSingleLine         | 是否把提示文案作为单行显示        | false
 qrcv_isShowTipBackground         | 是否显示提示文案的背景        | false
 qrcv_tipBackgroundColor         | 提示文案的背景色        | #22000000
 qrcv_isOnlyDecodeScanBoxArea         | 是否只识别扫描框中的码        | false
 qrcv_isShowLocationPoint         | 是否显示定位点        | false
 qrcv_isAutoZoom         | 码太小时是否自动缩放        | false


## 接口说明

>QRCodeView

```java

/**
 * ZBarView 设置识别的格式。详细用法请看 entry 的 ZbarTestScanAbilitySlice 中的 onClick 方法
 *
 * @param barcodeType 识别的格式
 * @param formatList  barcodeType 为 BarcdeType.CUSTOM 时，必须指定该值
 */
public void setType(BarcodeType barcodeType, List<BarcodeFormat> formatList)

/**
 * ZXingView 设置识别的格式。详细用法请看ZxingTestScanAbilitySlice中的 onClick 方法
 *
 * @param barcodeType 识别的格式
 * @param hintMap     barcodeType 为 BarcodeType.CUSTOM 时，必须指定该值
 */
public void setType(BarcodeType barcodeType, Map<DecodeHintType, Object> hintMap)

/**
 * 设置扫描二维码的代理
 *
 * @param delegate 扫描二维码的代理
 */
public void setDelegate(Delegate delegate)

/**
 * 显示扫描框
 */
public void showScanRect()

/**
 * 隐藏扫描框
 */
public void hiddenScanRect()

/**
 * 打开后置摄像头开始预览，但是并未开始识别
 */
public void startCamera()

/**
 * 打开指定摄像头开始预览，但是并未开始识别
 *
 * @param cameraFacing  Camera.CameraInfo.CAMERA_FACING_BACK or Camera.CameraInfo.CAMERA_FACING_FRONT
 */
public void startCamera(int cameraFacing)

/**
 * 关闭摄像头预览，并且隐藏扫描框
 */
public void stopCamera()

/**
 * 开始识别
 */
public void startSpot()

/**
 * 停止识别
 */
public void stopCallBack()

/**
 * 停止识别，并且隐藏扫描框
 */
public void stopSpotAndHiddenRect()

/**
 * 显示扫描框，并开始识别
 */
public void startSpotAndShowRect()

/**
 * 打开闪光灯
 */
public void openFlashlight()

/**
 * 关闭散光灯
 */
public void closeFlashlight()


>QRCodeView.Delegate   扫描二维码的代理

```java
/**
 * 处理扫描结果
 *
 * @param result 摄像头扫码时只要回调了该方法 result 就一定有值，不会为 null。解析本地图片或 Bitmap 时 result 可能为 null
 */
void onScanQRCodeSuccess(String result)


/**
 * 处理打开相机出错
 */
void onScanQRCodeOpenCameraError()
```

>QRCodeEncoder  创建二维码图片。几个重载方法都是耗时操作，请在子线程中调用。

```java
/**
 * 同步创建黑色前景色、白色背景色的二维码图片。该方法是耗时操作，请在子线程中调用。
 *
 * @param content 要生成的二维码图片内容
 * @param size    图片宽高，单位为px
 */
public static PixelMap syncEncodeQRCode(String content, int size)

/**
 * 同步创建指定前景色、白色背景色的二维码图片。该方法是耗时操作，请在子线程中调用。
 *
 * @param content         要生成的二维码图片内容
 * @param size            图片宽高，单位为px
 * @param foregroundColor 二维码图片的前景色
 */
public static PixelMap syncEncodeQRCode(String content, int size, int foregroundColor)

/**
 * 同步创建指定前景色、白色背景色、带logo的二维码图片。该方法是耗时操作，请在子线程中调用。
 *
 * @param content         要生成的二维码图片内容
 * @param size            图片宽高，单位为px
 * @param foregroundColor 二维码图片的前景色
 * @param logo            二维码图片的logo
 */
public static PixelMap syncEncodeQRCode(String content, int size, int foregroundColor, Bitmap logo)

/**
 * 同步创建指定前景色、指定背景色、带logo的二维码图片。该方法是耗时操作，请在子线程中调用。
 *
 * @param content         要生成的二维码图片内容
 * @param size            图片宽高，单位为px
 * @param foregroundColor 二维码图片的前景色
 * @param backgroundColor 二维码图片的背景色
 * @param logo            二维码图片的logo
 */
public static PixelMap syncEncodeQRCode(String content, int size, int foregroundColor, int backgroundColor, Bitmap logo)

/**
 * 同步创建条形码图片
 *
 * @param content  要生成条形码包含的内容
 * @param width    条形码的宽度，单位px
 * @param height   条形码的高度，单位px
 * @param textSize 字体大小，单位px，如果等于0则不在底部绘制文字
 */
public static PixelMap syncEncodeBarcode(String content, int width, int height, int textSize)
```


## License

    Copyright (C) 2012 The Android Open Source Project
    Copyright 2014 bingoogolapple

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
