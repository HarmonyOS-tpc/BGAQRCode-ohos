package com.bingoogolapple.qrcode.zxing;

import com.google.zxing.LuminanceSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Rect;

/**
 * Created by aaron on 16/7/27.
 * 自定义解析Bitmap LuminanceSource
 */
public class BitmapLuminanceSource extends LuminanceSource {
    private byte[] bitmapPixels;

    public BitmapLuminanceSource(PixelMap pixelMap) {
        super(pixelMap.getImageInfo().size.width, pixelMap.getImageInfo().size.height);

        // 首先，要取得该图片的像素数组内容
        int[] data = new int[getWidth() * getHeight()];
        this.bitmapPixels = new byte[getWidth() * getHeight()];
        pixelMap.readPixels(data, 0, getWidth(), new Rect(0, 0, getWidth(), getHeight()));
        // 将int数组转换为byte数组，也就是取像素值中蓝色值部分作为辨析内容
        for (int i = 0; i < data.length; i++) {
            this.bitmapPixels[i] = (byte) data[i];
        }
    }

    @Override
    public byte[] getMatrix() {
        // 返回我们生成好的像素数据
        return bitmapPixels;
    }

    @Override
    public byte[] getRow(int y1, byte[] row) {
        // 这里要得到指定行的像素数据
        System.arraycopy(bitmapPixels, y1 * getWidth(), row, 0, getWidth());
        return row;
    }
}
