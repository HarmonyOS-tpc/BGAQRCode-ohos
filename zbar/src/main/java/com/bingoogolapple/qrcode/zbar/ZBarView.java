package com.bingoogolapple.qrcode.zbar;

import com.bingoogolapple.qrcode.core.*;
import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import ohos.agp.components.AttrSet;
import ohos.agp.utils.Rect;
import ohos.app.Context;
import ohos.media.image.PixelMap;

import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * zBarView 解析扫码页面
 */
public class ZBarView extends QRCodeView {

    static {
        System.loadLibrary("iconv");
        System.loadLibrary("zbarjni");
    }

    private ImageScanner mScanner;
    private List<BarcodeFormat> mFormatList;

    public ZBarView(Context context) {
        super(context);
        setupReader();
    }

    public ZBarView(Context context, AttrSet attributeSet) {
        this(context, attributeSet, null);
    }

    public ZBarView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupReader();
    }

    protected void setupReader() {
        mScanner = new ImageScanner();
        mScanner.setConfig(0, Config.X_DENSITY, 3);
        mScanner.setConfig(0, Config.Y_DENSITY, 3);
        mScanner.setConfig(Symbol.NONE, Config.ENABLE, 0);
        for (BarcodeFormat format : getFormats()) {
            mScanner.setConfig(format.getId(), Config.ENABLE, 1);
        }
    }

    public Collection<BarcodeFormat> getFormats() {
        if (mBarcodeType == BarcodeType.ONE_DIMENSION) {
            return BarcodeFormat.ONE_DIMENSION_FORMAT_LIST;
        } else if (mBarcodeType == BarcodeType.TWO_DIMENSION) {
            return BarcodeFormat.TWO_DIMENSION_FORMAT_LIST;
        } else if (mBarcodeType == BarcodeType.ONLY_QR_CODE) {
            return Collections.singletonList(BarcodeFormat.QRCODE);
        } else if (mBarcodeType == BarcodeType.ONLY_CODE_128) {
            return Collections.singletonList(BarcodeFormat.CODE128);
        } else if (mBarcodeType == BarcodeType.ONLY_EAN_13) {
            return Collections.singletonList(BarcodeFormat.EAN13);
        } else if (mBarcodeType == BarcodeType.HIGH_FREQUENCY) {
            return BarcodeFormat.HIGH_FREQUENCY_FORMAT_LIST;
        } else if (mBarcodeType == BarcodeType.CUSTOM) {
            return mFormatList;
        } else {
            return BarcodeFormat.ALL_FORMAT_LIST;
        }
    }
    /**
     * 设置识别的格式
     *
     * @param barcodeType 识别的格式
     * @param formatList  barcodeType 为 BarcodeType.CUSTOM 时，必须指定该值
     */
    public void setType(BarcodeType barcodeType, List<BarcodeFormat> formatList) {
        mBarcodeType = barcodeType;
        mFormatList = formatList;
        if (mBarcodeType == BarcodeType.CUSTOM && (mFormatList == null || mFormatList.isEmpty())) {
            throw new RuntimeException("barcodeType 为 BarcodeType.CUSTOM 时 formatList 不能为空");
        }
        setupReader();
    }

    @Override
    protected ScanResult processData(byte[] data, int width, int height, boolean isRetry) {
        Image barcode = new Image(width, height, "Y800");

        Rect scanBoxAreaRect = mScanBoxView.getScanBoxAreaRect(height);
        if (scanBoxAreaRect != null && !isRetry && scanBoxAreaRect.left + scanBoxAreaRect.getWidth() <= width
                && scanBoxAreaRect.top + scanBoxAreaRect.getHeight() <= height) {
            barcode.setCrop(scanBoxAreaRect.left, scanBoxAreaRect.top, scanBoxAreaRect.getWidth(), scanBoxAreaRect.getHeight());
        }

        barcode.setData(data);
        String result = processData(barcode);
        return new ScanResult(result);
    }


    private String processData(Image barcode) {
        if (mScanner.scanImage(barcode) == 0) {
            return null;
        }

        for (Symbol symbol : mScanner.getResults()) {
            // 未能识别的格式继续遍历
            if (symbol.getType() == Symbol.NONE) {
                continue;
            }

            String symData;
            symData = new String(symbol.getDataBytes(), StandardCharsets.UTF_8);
            LogUtil.error("解析结果是symData:" + symData);
            // 空数据继续遍历
            if (TextUtils.isEmpty(symData)) {
                continue;
            }

            // 处理自动缩放和定位点
            boolean isNeedAutoZoom = isNeedAutoZoom(symbol);
            if (isShowLocationPoint() || isNeedAutoZoom) {
                if (transformToViewCoordinates(symbol.getLocationPoints(), null, isNeedAutoZoom, symData)) {
                    return null;
                } else {
                    return symData;
                }
            } else {
                return symData;
            }
        }
        return null;
    }

    private boolean isNeedAutoZoom(Symbol symbol) {
        return isAutoZoom() && symbol.getType() == Symbol.QRCODE;
    }

    @Override
    protected ScanResult processBitmapData(PixelMap bitmap) {
        try {
            int picWidth = bitmap.getImageInfo().size.width;
            int picHeight = bitmap.getImageInfo().size.height;
            Image barcode = new Image(picWidth, picHeight, "RGB4");
            int[] pix = new int[picWidth * picHeight];
            bitmap.readPixels(pix, 0, picWidth, new ohos.media.image.common.Rect(0, 0, picWidth, picHeight));
            barcode.setData(pix);
            String result = processData(barcode.convert("Y800"));
            return new ScanResult(result);
        } catch (Exception e) {
            LogUtil.error(e.getMessage());
            return null;
        }
    }
}