//
// Created by cWX1000561 on 2020/11/11.
//

#ifndef ijkplayer_hos_ohos_log_H
#define ijkplayer_hos_ohos_log_H

#define OHOSIJK_LOG_TAG    "charlie_native"
enum IjkLogLevel {
    IL_INFO,
    IL_ERROR
};
#ifdef IJKDEBUG
#define OHLOGD(...) __ohos_log_print_debug(IL_INFO ,OHOSIJK_LOG_TAG,__FILE__,__LINE__,__VA_ARGS__)
#define OHLOGI(...) __ohos_log_print_debug(IL_INFO ,OHOSIJK_LOG_TAG, __FILE__,__LINE__,__VA_ARGS__)
#define OHLOGW(...) __ohos_log_print_debug(IL_ERROR ,OHOSIJK_LOG_TAG,__FILE__,__LINE__, __VA_ARGS__)
#define OHLOGE(...) __ohos_log_print_debug(IL_ERROR ,OHOSIJK_LOG_TAG,__FILE__,__LINE__,__VA_ARGS__)
#define OHLOGF(...) __ohos_log_print_debug(IL_ERROR ,OHOSIJK_LOG_TAG,__FILE__,__LINE__,__VA_ARGS__)
#else
#define OHLOGD(...) __ohos_log_print(IL_INFO ,OHOSIJK_LOG_TAG, __VA_ARGS__)
#define OHLOGI(...) __ohos_log_print(IL_INFO ,OHOSIJK_LOG_TAG, __VA_ARGS__)
#define OHLOGW(...) __ohos_log_print(IL_ERROR ,OHOSIJK_LOG_TAG, __VA_ARGS__)
#define OHLOGE(...) __ohos_log_print(IL_ERROR ,OHOSIJK_LOG_TAG,__VA_ARGS__)
#define OHLOGF(...) __ohos_log_print(IL_ERROR ,OHOSIJK_LOG_TAG,__VA_ARGS__)
#endif

#define OHOS_LOG_BUF_SIZE (4096)
#ifdef __cplusplus
extern "C" {
#endif
void __ohos_log_print(enum IjkLogLevel level, const char* tag, const char* fmt, ...);
void __ohos_log_print_debug(enum IjkLogLevel level, const char* tag,const char* file,int line, const char* fmt, ...);
#ifdef __cplusplus
}
#endif
#endif//ijkplayer_hos_ohos_log.h_H
