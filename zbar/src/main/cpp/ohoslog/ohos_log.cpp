#include "ohos_log.h"
#include <stdio.h>

#include <hilog/log.h>

void __ohos_log_print(enum IjkLogLevel level,const char* tag,const char* fmt,...)
{
    char buf[OHOS_LOG_BUF_SIZE] = {0};
    va_list arg;
    va_start(arg, fmt);
    vsnprintf(buf, OHOS_LOG_BUF_SIZE,fmt, arg);
    HiLogPrint(LOG_APP,LOG_ERROR,LOG_DOMAIN,tag,"%{public}s",buf);
    va_end(arg);
}

void __ohos_log_print_debug(enum IjkLogLevel level, const char* tag,const char* file,int line, const char* fmt, ...)
{
    char buf[OHOS_LOG_BUF_SIZE] = {0};
    va_list arg;
    va_start(arg, fmt);
    vsnprintf(buf, OHOS_LOG_BUF_SIZE,fmt, arg);
    HiLogPrint(LOG_APP,LOG_ERROR,LOG_DOMAIN,tag,"%{public}s:%{public}d %{public}s",file,line,buf);
    va_end(arg);
}