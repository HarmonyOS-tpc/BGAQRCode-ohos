/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bingoogolapple.qrcode.slice;

import com.bingoogolapple.qrcode.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;

/**
 * zbar跳转界面
 */
public class ZbarAbilitySlice extends AbilitySlice {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_zbar_layout);

        (findComponentById(ResourceTable.Id_enter_zbar))
                .setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        Intent intent1 = new Intent();
                        Operation operation = new Intent.OperationBuilder()
                                .withBundleName("com.bingoogolapple.qrcode")
                                .withAbilityName("com.bingoogolapple.qrcode.ZbarTestScanAbility")
                                .withDeviceId("")
                                .build();
                        intent1.setOperation(operation);
                        startAbility(intent1);
                    }
                });

    }

}
