/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bingoogolapple.qrcode.slice;

import com.bingoogolapple.qrcode.ResourceTable;
import com.bingoogolapple.qrcode.core.BGAQRCodeUtil;
import com.bingoogolapple.qrcode.zxing.QRCodeDecoder;
import com.bingoogolapple.qrcode.zxing.QRCodeEncoder;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.ToastDialog;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.media.image.PixelMap;

/**
 * zxing页面扫描
 */
public class ZxingTestGenerateAbilitySlice extends AbilitySlice {

    private Image mChineseIv;
    private Image mEnglishIv;
    private Text mChineseBt;
    private Text mEnglishBt;
    private Image mChineseLogoIv;
    private Image mEnglishLogoIv;
    private Text mChineseLogoBt;
    private Text mEnglishLogoBt;
    private Image mWithContentIv;
    private Image mWithoutContentIv;
    private Text mWithContentBt;
    private Text mWithoutContentBt;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_zxinggen_layout);
        initView();
        createQRCode();
    }

    private void initView() {
        mChineseIv = (Image) findComponentById(ResourceTable.Id_iv_chinese);
        mEnglishIv = (Image) findComponentById(ResourceTable.Id_iv_english);
        mChineseBt = (Text) findComponentById(ResourceTable.Id_tx_chinese);
        mEnglishBt = (Text) findComponentById(ResourceTable.Id_tx_english);

        mChineseLogoIv = (Image) findComponentById(ResourceTable.Id_iv_chinese_logo);
        mEnglishLogoIv = (Image) findComponentById(ResourceTable.Id_iv_english_logo);
        mChineseLogoBt = (Text) findComponentById(ResourceTable.Id_tx_chinese_logo);
        mEnglishLogoBt = (Text) findComponentById(ResourceTable.Id_tx_english_logo);

        mWithContentIv = (Image) findComponentById(ResourceTable.Id_iv_with_content);
        mWithoutContentIv = (Image) findComponentById(ResourceTable.Id_iv_without_content);
        mWithContentBt = (Text) findComponentById(ResourceTable.Id_tx_with_content);
        mWithoutContentBt = (Text) findComponentById(ResourceTable.Id_tx_without_content);

        mChineseBt.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                decodeChinese();
            }
        });

        mEnglishBt.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                decodeEnglish();
            }
        });

        mChineseLogoBt.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                decodeChineseLogo();
            }
        });

        mEnglishLogoBt.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                decodeEnglishLogo();
            }
        });

        mWithContentBt.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                decodeChineseLogo();
            }
        });

        mWithoutContentBt.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                decodeEnglishLogo();
            }
        });
    }

    /**
     * 解析中文二维码
     */
    public void decodeChinese() {
        PixelMap bitmap = mChineseIv.getPixelMap();
        decode(bitmap, "解析中文二维码失败");
    }

    /**
     * 解析英文二维码
     */
    public void decodeEnglish() {
        PixelMap bitmap = mEnglishIv.getPixelMap();
        decode(bitmap, "解析英文二维码失败");
    }

    /**
     * 解析中文带logo二维码
     */
    public void decodeChineseLogo() {
        PixelMap bitmap = mChineseLogoIv.getPixelMap();
        decode(bitmap, "解析中文带LOGO二维码失败");
    }

    /**
     * 解析英文带logo二维码
     */
    public void decodeEnglishLogo() {
        PixelMap bitmap = mEnglishLogoIv.getPixelMap();
        decode(bitmap, "解析英文带LOGO二维码失败");
    }

    /**
     * 识别底部带文字条形码
     */
    public void decodeWithContent() {
        PixelMap bitmap = mWithoutContentIv.getPixelMap();
        decode(bitmap, "识别底部带文字的条形码失败");
    }

    /**
     * 识别底部不带文字的条形码
     */
    public void decodeWithoutContent() {
        PixelMap bitmap = mWithoutContentIv.getPixelMap();
        decode(bitmap, "识别底部没带文字的条形码失败");
    }

    private void decode(final PixelMap bitmap, final String errorTip) {
        new EventHandler(EventRunner.create("decode")).postTask(() -> {
            String res = QRCodeDecoder.syncDecodeQRCode(bitmap);
            getUITaskDispatcher().asyncDispatch(() -> {
                ToastDialog toastDialog = new ToastDialog(getContext());
                toastDialog.setOffset(0,100) ;
                toastDialog.setContentText(res);
                toastDialog.show();
            });
        });
    }

    private void createQRCode() {
        createChineseQRCode();
        createEnglishQRCode();
        createChineseQRCodeWithLogo();
        createEnglishQRCodeWithLogo();
        createWidthContent();
        createWithoutContent();
    }

    private void createEnglishQRCode() {
        new EventHandler(EventRunner.create("createEnglishQRCode")).postTask(() -> {
            PixelMap pixelMap = QRCodeEncoder.syncEncodeQRCode("bingoogolapple", BGAQRCodeUtil.dp2px(ZxingTestGenerateAbilitySlice.this, 150));
            getUITaskDispatcher().asyncDispatch(() -> {
                mEnglishIv.setPixelMap(pixelMap);
            });
        });
    }

    private void createChineseQRCode() {
        new EventHandler(EventRunner.create("createChineseQRCode")).postTask(() -> {
            PixelMap pixelMap = QRCodeEncoder.syncEncodeQRCode("王浩", BGAQRCodeUtil.dp2px(ZxingTestGenerateAbilitySlice.this, 150));
            getUITaskDispatcher().asyncDispatch(() -> {
                mChineseIv.setPixelMap(pixelMap);
            });
        });
    }

    private void createEnglishQRCodeWithLogo() {
        new EventHandler(EventRunner.create("createEnglishQRCodeWithLogo")).postTask(() -> {
            PixelMap pixelMap =  QRCodeEncoder.syncEncodeQRCode("bingoogolapple", BGAQRCodeUtil.dp2px(ZxingTestGenerateAbilitySlice.this, 150), Color.getIntColor("#ff0000"), mEnglishLogoIv.getPixelMap());
            getUITaskDispatcher().asyncDispatch(() -> {
                mEnglishLogoIv.setPixelMap(pixelMap);
            });
        });
    }

    private void createChineseQRCodeWithLogo() {
        new EventHandler(EventRunner.create("createChineseQRCodeWithLogo")).postTask(() -> {
            PixelMap pixelMap =  QRCodeEncoder.syncEncodeQRCode("王浩", BGAQRCodeUtil.dp2px(ZxingTestGenerateAbilitySlice.this, 150), Color.getIntColor("#ff0000"), mEnglishLogoIv.getPixelMap());
            getUITaskDispatcher().asyncDispatch(() -> {
                mChineseLogoIv.setPixelMap(pixelMap);
            });
        });
    }

    private void createWidthContent() {
        new EventHandler(EventRunner.create("createWidthContent")).postTask(() -> {
            int width = BGAQRCodeUtil.dp2px(ZxingTestGenerateAbilitySlice.this, 150);
            int height = BGAQRCodeUtil.dp2px(ZxingTestGenerateAbilitySlice.this, 70);
            int textSize = BGAQRCodeUtil.sp2px(ZxingTestGenerateAbilitySlice.this, 14);
            PixelMap pixelMap = QRCodeEncoder.syncEncodeBarcode("bga123", width, height, textSize);
            getUITaskDispatcher().asyncDispatch(() -> {
                mWithContentIv.setPixelMap(pixelMap);
            });
        });
    }

    private void createWithoutContent() {
        new EventHandler(EventRunner.create("createWithoutContent")).postTask(() -> {
            int width = BGAQRCodeUtil.dp2px(ZxingTestGenerateAbilitySlice.this, 150);
            int height = BGAQRCodeUtil.dp2px(ZxingTestGenerateAbilitySlice.this, 70);
            PixelMap pixelMap = QRCodeEncoder.syncEncodeBarcode("bingoogolapple123", width, height, 0);
            getUITaskDispatcher().asyncDispatch(() -> {
                mWithoutContentIv.setPixelMap(pixelMap);
            });
        });
    }
}
