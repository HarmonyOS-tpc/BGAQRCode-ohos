/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bingoogolapple.qrcode.slice;

import com.bingoogolapple.qrcode.ResourceTable;
import com.bingoogolapple.qrcode.core.BarcodeType;
import com.bingoogolapple.qrcode.core.LogUtil;
import com.bingoogolapple.qrcode.core.QRCodeView;
import com.bingoogolapple.qrcode.core.TextUtils;
import com.bingoogolapple.qrcode.zbar.BarcodeFormat;
import com.bingoogolapple.qrcode.zbar.ZBarView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import ohos.app.dispatcher.task.TaskPriority;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.vibrator.agent.VibratorAgent;

import java.util.ArrayList;
import java.util.List;

/**
 * zbar扫描二维码页面
 */
public class ZbarTestScanAbilitySlice extends AbilitySlice implements Component.ClickedListener, QRCodeView.Delegate {

    private ZBarView mZBarView;
    private Text text2;


    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_zbarscan_layout);
        getWindow().setTransparent(true);

        mZBarView = (ZBarView) findComponentById(ResourceTable.Id_zbarview);
        mZBarView.setDelegate(this);
        text2 = (Text) findComponentById(ResourceTable.Id_text2);
        findComponentById(ResourceTable.Id_start_spot).setClickedListener(this);
        findComponentById(ResourceTable.Id_show_scan_rect).setClickedListener(this);
        findComponentById(ResourceTable.Id_hidden_scan_rect).setClickedListener(this);
        findComponentById(ResourceTable.Id_stop_spot).setClickedListener(this);

        findComponentById(ResourceTable.Id_decode_full_screen_area).setClickedListener(this);
        findComponentById(ResourceTable.Id_open_flashlight).setClickedListener(this);
        findComponentById(ResourceTable.Id_close_flashlight).setClickedListener(this);
        findComponentById(ResourceTable.Id_scan_one_dimension).setClickedListener(this);

        findComponentById(ResourceTable.Id_scan_two_dimension).setClickedListener(this);
        findComponentById(ResourceTable.Id_scan_qr_code).setClickedListener(this);
        findComponentById(ResourceTable.Id_scan_code128).setClickedListener(this);
        findComponentById(ResourceTable.Id_scan_ean13).setClickedListener(this);

        findComponentById(ResourceTable.Id_scan_high_frequency).setClickedListener(this);
        findComponentById(ResourceTable.Id_scan_all).setClickedListener(this);
        findComponentById(ResourceTable.Id_scan_custom).setClickedListener(this);
        findComponentById(ResourceTable.Id_choose_qrcde_from_gallery).setClickedListener(this);


        LogUtil.info("TestZbarAbilitySlice...");
    }

    @Override
    protected void onActive() {
        super.onActive();
        mZBarView.startSpotAndShowRect();
        mZBarView.setDelegate(this);
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        mZBarView.onDestroy();
    }

    private void vibrate() {
        VibratorAgent vibratorAgent = new VibratorAgent();
        List<Integer> vids = vibratorAgent.getVibratorIdList();
        LogUtil.info("vibrate size:" + vids.size());
        for (Integer position : vids) {
            LogUtil.info("vibrate id:" + vids);
            if (vibratorAgent.isSupport(position)) {
                vibratorAgent.startOnce(position, 200);
                break;
            }
        }
    }

    void setTitle(String title) {
        String res = "未识别";
        if (!TextUtils.isEmpty(title)) {
            res = title;
        }
        showTips(getContext(), res);


    }

    private void showTips(Context context, String message) {
        getUITaskDispatcher().asyncDispatch(() -> {
            text2.setText(message);
        });
    }

    @Override
    public void onScanQRCodeSuccess(String result) {
        LogUtil.error("onScanQRCodeSuccess....");
        mZBarView.startCamera(); // 开始识别
        if (TextUtils.isEmpty(result)) {

            return;
        }
        LogUtil.error("result:" + result);
        setTitle(result);
        vibrate();


    }

    @Override
    public void onCameraAmbientBrightnessChanged(boolean isDark) {
        // 这里是通过修改提示文案来展示环境是否过暗的状态，接入方也可以根据 isDark 的值来实现其他交互效果
        String tipText = mZBarView.getScanBoxView().getTipText();
        String ambientBrightnessTip = "\n环境过暗，请打开闪光灯";
        if (isDark) {
            if (!tipText.contains(ambientBrightnessTip)) {
                mZBarView.getScanBoxView().setTipText(tipText + ambientBrightnessTip);
            }
        } else {
            if (tipText.contains(ambientBrightnessTip)) {
                tipText = tipText.substring(0, tipText.indexOf(ambientBrightnessTip));
                mZBarView.getScanBoxView().setTipText(tipText);
            }
        }
    }


    @Override
    public void onScanQRCodeOpenCameraError() {
        LogUtil.info("openCameraError");
    }
    /**
     * 按钮点击事件
     *
     * @param component 点击的控件
     */
    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_start_spot: // 开始识别
                mZBarView.startSpotAndShowRect(); // 显示扫描框
                mZBarView.setDelegate(this);
                break;
            case ResourceTable.Id_stop_spot: // 停止识别
                mZBarView.stopCallBack();
                break;
            case ResourceTable.Id_show_scan_rect:
                mZBarView.showScanRect(); // 显示扫描框
                break;
            case ResourceTable.Id_hidden_scan_rect:
                mZBarView.hiddenScanRect(); // 隐藏扫描框
                break;

            case ResourceTable.Id_decode_full_screen_area:
                mZBarView.getScanBoxView().setOnlyDecodeScanBoxArea(false); // 识别整个屏幕中的码
                break;
            case ResourceTable.Id_open_flashlight:
                mZBarView.openFlashlight(); // 打开闪光灯
                break;
            case ResourceTable.Id_close_flashlight:
                mZBarView.closeFlashlight(); // 关闭闪光灯
                break;
            case ResourceTable.Id_scan_one_dimension:
                mZBarView.changeToScanBarcodeStyle(); // 切换成扫描条码样式
                mZBarView.setType(BarcodeType.ONE_DIMENSION, null); // 只识别 CODE_128
                mZBarView.startSpotAndShowRect(); // 显示扫描框，并开始识别
                break;
            case ResourceTable.Id_scan_two_dimension:
                mZBarView.changeToScanQRCodeStyle(); // 切换成扫描二维码样式
                mZBarView.setType(BarcodeType.TWO_DIMENSION, null); // 识别所有类型的码
                mZBarView.startSpotAndShowRect(); // 显示扫描框，并开始识别
                break;
            case ResourceTable.Id_scan_qr_code:
                mZBarView.changeToScanQRCodeStyle(); // 切换成扫描二维码样式
                mZBarView.setType(BarcodeType.ONLY_QR_CODE, null); // 识别所有类型的码
                mZBarView.startSpotAndShowRect(); // 显示扫描框，并开始识别
                break;
            case ResourceTable.Id_scan_code128:
                mZBarView.changeToScanBarcodeStyle(); // 切换成扫描条码样式
                mZBarView.setType(BarcodeType.ONLY_CODE_128, null); // 只识别 CODE_128
                mZBarView.startSpotAndShowRect(); // 显示扫描框，并开始识别
                break;
            case ResourceTable.Id_scan_ean13:
                mZBarView.changeToScanBarcodeStyle(); // 切换成扫描条码样式
                mZBarView.setType(BarcodeType.ONLY_EAN_13, null); // 只识别 CODE_128
                mZBarView.startSpotAndShowRect(); // 显示扫描框，并开始识别
                break;
            case ResourceTable.Id_scan_high_frequency:
                mZBarView.changeToScanQRCodeStyle(); // 切换成扫描二维码样式
                mZBarView.setType(BarcodeType.HIGH_FREQUENCY, null); // 只识别高频率格式，包括 QR_CODE、ISBN13、UPC_A、EAN_13、CODE_128
                mZBarView.startSpotAndShowRect(); // 显示扫描框，并开始识别
                break;
            case ResourceTable.Id_scan_all:
                mZBarView.changeToScanQRCodeStyle(); // 切换成扫描二维码样式
                mZBarView.setType(BarcodeType.ALL, null); // 识别所有类型的码
                mZBarView.startSpotAndShowRect(); // 显示扫描框，并开始识别
                break;
            case ResourceTable.Id_scan_custom:
                zbarTestScanCustom();
                break;
            default:
                break;
        }
    }

    private void zbarTestScanCode(BarcodeType onlyCode128) {
        mZBarView.changeToScanBarcodeStyle(); // 切换成扫描条码样式
        List<BarcodeFormat> formatList = new ArrayList<>();
        formatList.add(BarcodeFormat.EAN13);
        mZBarView.setType(onlyCode128, formatList); // 只识别 CODE_128
        mZBarView.startSpotAndShowRect(); // 显示扫描框，并开始识别
    }

    private void zbarTestScanAll(BarcodeType all) {
        mZBarView.changeToScanQRCodeStyle(); // 切换成扫描二维码样式
        List<BarcodeFormat> formatList = new ArrayList<>();
        formatList.add(BarcodeFormat.QRCODE);
        formatList.add(BarcodeFormat.ISBN13);
        formatList.add(BarcodeFormat.UPCA);
        formatList.add(BarcodeFormat.EAN13);
        formatList.add(BarcodeFormat.PDF417);
        formatList.add(BarcodeFormat.CODE128);
        mZBarView.setType(all, formatList); // 识别所有类型的码
        mZBarView.startSpotAndShowRect(); // 显示扫描框，并开始识别
    }

    private void zbarTestScanCustom() {
        mZBarView.changeToScanQRCodeStyle(); // 切换成扫描二维码样式

        List<BarcodeFormat> formatList = new ArrayList<>();
        formatList.add(BarcodeFormat.QRCODE);
        formatList.add(BarcodeFormat.ISBN13);
        formatList.add(BarcodeFormat.UPCA);
        formatList.add(BarcodeFormat.EAN13);
        formatList.add(BarcodeFormat.CODE128);
        mZBarView.setType(BarcodeType.CUSTOM, formatList); // 自定义识别的类型

        mZBarView.startSpotAndShowRect(); // 显示扫描框，并开始识别
    }
}
